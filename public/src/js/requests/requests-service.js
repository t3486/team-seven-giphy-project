import { credentials } from '../common/credentials.js';
import { NotificationManager } from '../views/NotificationManager.js';

const notification = new NotificationManager();

/** fetches trending gifs
   * @param { number } number number of wanted gifs to fetch, 
   * default is 10
* */
export const getTrendingGifs = (number = 10) => {
  return fetch(credentials.URL + '/trending?' + new URLSearchParams({
    api_key: credentials.API_KEY,
    limit: number,
  }))
    .then(response => response.json())
    .then(response => response.data);
};

/** fetches gifs by passed several ids
   * @param { string } favoriteIds comma separated string, 
   * contains gifs ids
* */
export const getGifsById = (favoriteIds) => {
  return fetch(credentials.URL + '?' + new URLSearchParams({
    api_key: credentials.API_KEY,
    ids: favoriteIds,
  }))
    .then(response => response.json())
    .then(response => response.data);
};

/** fetches searched gif by passed search input from the user
   * @param { string } searchKey entered input value
* */
export const getSearchedGifs = (searchKey) => {
  return fetch(credentials.URL + '/search?' + new URLSearchParams({
    api_key: credentials.API_KEY,
    q: searchKey,
    limit: 20,
  }))
    .then(response => response.json())
    .then(response => response.data);
};

/** fetches gif by passed id
   * @param { string } id id of the gif
   * @returns { Promise } 
* */
export const getGifById = (id) => {
  return fetch(credentials.URL + `/${id}?` + new URLSearchParams({
    api_key: credentials.API_KEY,
    gif_id: id,
  }))
    .then(response => response.json())
    .then(response => response.data);
};

/** fetches random gif by passed id
   * @param { string } id id of the gif
   * @returns { Promise } 
* */
export const getRandomGif = () => {
  return fetch(credentials.URL + `/random?` + new URLSearchParams({
    api_key: credentials.API_KEY,
  }))
    .then(response => response.json())
    .then(response => response.data);
};

export const uploadGif = (file) => {
  const formData = new FormData();
  const loadingContainer = document.getElementById('upload-loading');

  loadingContainer.style.display = 'grid';
  formData.append('file', file);
  return fetch('https://upload.giphy.com/v1/gifs?' + new URLSearchParams({ api_key: 'Mrl5sys01TV9aEGJZDgD7PxMus1XGnsl' }),
    { method: 'POST', body: formData })
    .then(response => {
      if (response.status !== 200) throw new Error('Incorrect type file!');
      else return response.json();
    }).finally(() => loadingContainer.style.display = 'none');
};

