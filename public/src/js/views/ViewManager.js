export class ViewManager {
  #mappedViews = new Map();

  /** adds mapped view by passed key
   * @param { string } key
  * */
  addViewMapping(key, view) {
    this.#mappedViews.set(key, view);
  }

  /** removes mapped view by passed key
   * @param { string } key
   * */
  removeViewMapping(key) {
    if (this.#mappedViews.has(key)) this.#mappedViews.delete(key);
  }

  /** executes the mapped function by passed key
   * @param { string } key
   * @param { any } params
   * */
  execute(key, params) {
    if (this.#mappedViews.has(key) && params !== null) return this.#mappedViews.get(key)(params);
    if (this.#mappedViews.has(key)) this.#mappedViews.get(key)();
  }
}
