import { gifSection } from './gif-section.js';
import { infiniteView } from './infinite-view.js';

export const homeView = (sections, random) => {
  return sections.map(gifSection) + `<h2 class="random-heading">Random</h2>` + infiniteView(random);
};
