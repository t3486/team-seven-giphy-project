import { gifCard } from './general-view.js';

export const infiniteView = (category) => `
<div class="general-container infinite-section">
  <div class="search-content"> ${ category.gifs.map(gifCard) } </div>
</div>
`;
