import { gifCard } from './general-view.js';

export const uploadView = (section) => {
  return `
     <div class="w-100 h-100 d-flex flex-column align-items-center">
        <form class="form-container" id="form-upload-gif">
          <fieldset class="p-5">
            <legend class="form-heading"> Upload your favorite GIF! </legend>
            <div class="mb-3">
              <label for="git-upload-input" class="form-label">Select GIF</label>
              <div class="d-flex">
                <input type="file" id="gif-upload-input" class="form-control" placeholder="">
                <button type="submit" class="btn btn-primary upload-btn">Upload</button>
              </div>  
            </div>
          </fieldset>
        </form>
        <div class="uploaded-container d-flex flex-wrap gap-2 m-auto">
            ${ section.gifs.map(gifCard) }  
        </div>
        <div class="loading-container" id="upload-loading">
             <img src="../../resources/loader-spinner.gif" alt="loading animation">
        </div>
    </div>
`;
};
