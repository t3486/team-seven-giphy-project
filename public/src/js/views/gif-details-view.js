import { gifCard } from './gif-section.js';

export const gifDetailsView = (gif) => `
<div class="gif-detailed">
  <div class="image-container">
  ${gifCard(gif)}
  </div>
  <div class="info-container">
    <p>GIF Title: ${gif.title}</p>
    <p>User: ${gif.username}</p>
    <p>Rating: ${gif.rating}</p>
    <p>Uploaded on: ${gif['import_datetime']}</p>
  </div>
</div>
`;
