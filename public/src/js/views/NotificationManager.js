export class NotificationManager {

  #notification = null;
  #element = null;

  constructor() {
    this.#element = document.getElementById('notification-section');
    this.#notification = new bootstrap.Toast(this.#element);
  }

  /** shows warning notification message by passed message
  * @param { string } key
  */
  showWarning(message) {
    ['bg-success'].forEach(color => this.#element.classList.remove(color));
    this.#element.children[0].innerHTML = message;
    this.#element.classList.add('bg-danger');
    this.#show();
  }

  /** shows successful notification message by passed message
  * @param { string } key
  */
  showSuccess(message) {
    ['bg-danger'].forEach(color => this.#element.classList.remove(color));
    this.#element.classList.add('bg-success');
    this.#element.children[0].innerHTML = message;
    this.#show();
  }

  #show() {
    this.#element.classList.add('bg-dander');
    this.#notification.show();
  }
}
