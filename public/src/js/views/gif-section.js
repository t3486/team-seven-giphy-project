import { calcItemsInSection, getRandomColor, isActive } from './../events/helpers.js';
export const gifSection = (section) => {
  return `
    <div class="d-flex flex-column w-100">
      <h2> ${ section.title } </h2>
      <div class="swiped swiper" id="${ section.id }" data-swiped-item-count="${ calcItemsInSection(section.gifs.length) }">
          <div class="swiped-items" id="swiped-items">
              ${ section.gifs.map(gif => gifCard(gif)) }
          </div>
          <span class="arrow arrow-left" data-swiped-back="${section.id}"> <i class="fas fa-chevron-circle-left fa-3x"></i> </span>
          <span class="arrow arrow-right" data-swiped-next="${section.id}"> <i class="fas fa-chevron-circle-right fa-3x"></i> </span>
      </div>
    </div>`;
};

export const gifCard = (gif) => {
  return `<div class="swiped-item rounded-3 overflow-hidden" id="swiped-item" style="--random-color:${ getRandomColor() }">
              <img src="${gif.images.downsized_medium.url}" data-page="details" data-gif-id="${gif.id}" class="gift-image" style="width: 100%; height: 100%" alt="">
              <div class="upper-cover">
                <span> ${ gif.title } </span>
              </div>
               <span class="like-heart ${isActive(gif.id) ? 'like-heart-active' : ''}" data-gif-id="${gif.id}"><i class="fas fa-heart fa-2x"></i></span>
          </div>`;
};

