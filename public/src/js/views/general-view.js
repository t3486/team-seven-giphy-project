import { isActive } from '../events/helpers.js';

export const generalView = (category) => `
<div class="general-container">
  <h1> ${ category.name } </h1>
  <div class="search-content"> ${ category.gifs.map(gifCard) } </div>
</div>
`;

export const gifCard = (gif) => {
    return `<div class="swiped-item search-card rounded-3 overflow-hidden" id="swiped-item">
              <img src="${gif.images.downsized_medium.url}" data-page="details" data-gif-id="${gif.id}" class="gift-image" style="width: 100%; height: 100%" alt="">
              <div class="upper-cover">
                <span> ${ gif.title } </span>
              </div>
               <span class="like-heart ${isActive(gif.id) ? 'like-heart-active' : ''}" data-gif-id="${gif.id}"><i class="fas fa-heart fa-2x"></i></span>
            </div>`;
};
