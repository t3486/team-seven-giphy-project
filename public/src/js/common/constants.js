export const CONTAINER_SELECTOR = '#main-container';
export const HOME = 'home';
export const FAVORITE = 'favorite';
export const UPLOAD = 'upload';
export const SEARCH = 'search';
export const DETAILS = 'details';
export const INFINITE = 'infinite';

