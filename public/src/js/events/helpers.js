import { NotificationManager } from '../views/NotificationManager.js';

const notification = new NotificationManager();

/**
 *  Selects the given element if the query matches
 *  @param { string } selector - query to be searched for
 *  @return { Element | null }
 * */
export const select = (selector) => document.querySelector(selector);


/**
 * Checks if the passed GIF id is stored in the local storage. If it's not it adds it or remove.
 * @param { string } id - GIF ID
 */
export const toggleHeart = (id) => {
  const favoriteIds = getFavoriteIds();
  const card = select(`span[data-gif-id="${id}"]`);
  card.classList.toggle('like-heart-active');

  card.classList.contains('like-heart-active') ? notification.showSuccess('Added to favorite :)') :
    notification.showWarning('Removed from favorite :(');

  if (favoriteIds.includes(id)) {
    const updated = favoriteIds.filter(gifId => gifId !== id);
    localStorage.setItem('favorite', JSON.stringify(updated));
  } else {
    favoriteIds.push(id);
    localStorage.setItem('favorite', JSON.stringify(favoriteIds));
  }
};


/**
 * Checks if the given GIF ID is stored as favorite
 * @param { string } id - GIF ID
 * @return { Boolean }
 */
export const isActive = (id) => getFavoriteIds().includes(id);


/**
 * Returns all GIF's ID which are stored as favorite
 * @return { array }
 * */
const getFavoriteIds = () => {
  return JSON.parse(localStorage.getItem('favorite')) || [];
};


/**
 * Adds GIF's ID to uploaded storage
 * @param { string } id
 * */
export const addUploadedGif = (id) => {
  const uploadedIds = getUploadedGifs();
  uploadedIds.push(id);
  localStorage.setItem('uploaded', JSON.stringify(uploadedIds));
};


/**
 * Returns all GIF's ids which are uploaded
 * @return { array }
 * */
export const getUploadedGifs = () => {
  return JSON.parse(localStorage.getItem('uploaded')) || [];
};


/**
 * Returns the number of cards to be displayed upon '#main-container' width
 * @return { number } number of cards to be displayed
 * */
export const calcItemsInSection = (gifsCount, cardWidth = 285) => {
  const count = Math.floor((select('#main-container').offsetWidth) / cardWidth);
  if (gifsCount < count) return gifsCount;

  return count >= 5 ? 5 : count;
};

/**
 * Generates and returns random color
 * @return { string } random color
 * */
export const getRandomColor = () => {
  const letters = '0123456789ABCDEF';
  let color = '#';

  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};


