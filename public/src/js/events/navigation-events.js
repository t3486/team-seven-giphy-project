import { homeView } from '../views/home-view.js';
import { getUploadedGifs, select } from './helpers.js';
import { HOME, CONTAINER_SELECTOR, FAVORITE, UPLOAD, SEARCH, DETAILS, INFINITE } from '../common/constants.js';
import { Swiped } from '../../css/swiped/swiped.js';
import { getTrendingGifs, getGifsById, getSearchedGifs, getRandomGif } from '../requests/requests-service.js';
import { ViewManager } from '../views/ViewManager.js';
import { uploadView } from '../views/upload-view.js';
import { gifDetailsView } from './../views/gif-details-view.js';
import { generalView } from '../views/general-view.js';
import { getGifById } from '../requests/requests-service.js';
import { NotificationManager } from '../views/NotificationManager.js';
import { infiniteView } from './../views/infinite-view.js';

const swipedInstance = new Swiped();
const notification = new NotificationManager();

const renderHomeView = () => {

  Promise.all([
    getTrendingGifs(20),
    getSearchedGifs('programming'),
    Promise.all(Array.from({ length: 16 }, () => getRandomGif()))
      .then(gifs => ({ name: 'Random', gifs }))
  ])
    .then(([trending, programming, random]) => {
      return [{ title: 'Trending', id: 'swiper-1', gifs: trending },
      { title: 'Programming', id: 'swiper-2', gifs: programming },
        random
      ];
    })
    .then(([trending, programming, random]) => {
      select(CONTAINER_SELECTOR).innerHTML = homeView([trending, programming], random);
      swipedInstance.attachEvents();
    });
};

const renderFavoriteView = () => {
  const favoriteGfsIds = JSON.parse(localStorage.getItem('favorite'));
  if (favoriteGfsIds === null || favoriteGfsIds.length === 0) {

    return Promise.all([
      getRandomGif(),
      getRandomGif(),
      getRandomGif(),
    ])
      .then(gifs => ({ name: 'Random', gifs }))
      .then(section => {
        select(CONTAINER_SELECTOR).innerHTML = generalView(section, section.gifs.length);
        notification.showWarning('There are no favorite gifs yet!');
      });
  };

  getGifsById(favoriteGfsIds.join(','))
    .then(gifs => ({ name: 'Favorites', gifs }))
    .then(section => {
      select(CONTAINER_SELECTOR).innerHTML = generalView(section, section.gifs.length);
    });
};

const renderUploadView = () => {

  if (getUploadedGifs().length !== 0) {
    getGifsById(getUploadedGifs().join(','))
      .then(gifs => {
        select(CONTAINER_SELECTOR).innerHTML = uploadView({ title: 'Uploaded GIFS', id: 'upload-section', gifs });
      });
  } else {
    select(CONTAINER_SELECTOR).innerHTML = uploadView({ title: 'No GIFS', gifs: [] });
  }
};

const renderSearchedGifs = () => {
  const searchInput = select('.form-control');

  getSearchedGifs(searchInput.value)
    .then(gifs => ({ name: `Search results for "${searchInput.value}"`, gifs }))
    .then(searchResult => {
      select(CONTAINER_SELECTOR).innerHTML = generalView(searchResult);
      console.log(searchResult);
      searchResult.gifs.length > 0 ? notification.showSuccess('Searched gifs loaded!') : notification.showWarning('There no gifs matching criteria!');
      searchInput.value = '';
    });
};

const renderGifDetails = (gifId) => {
  getGifById(gifId)
    .then(gif => {
      select(CONTAINER_SELECTOR).innerHTML = gifDetailsView(gif);
    });
};

const renderInfiniteSection = () => {
  return Promise.all(Array.from({ length: 16 }, () => getRandomGif()))
    .then(gifs => ({ name: 'Random', gifs }))
    .then(section => {
      const newElement = document.createElement('div');
      newElement.innerHTML = infiniteView(section)
      select(CONTAINER_SELECTOR).append(newElement);
    });
};

/** mapping all views */
const viewManager = new ViewManager();
viewManager.addViewMapping(HOME, renderHomeView);
viewManager.addViewMapping(INFINITE, renderInfiniteSection);
viewManager.addViewMapping(FAVORITE, renderFavoriteView);
viewManager.addViewMapping(UPLOAD, renderUploadView);
viewManager.addViewMapping(SEARCH, renderSearchedGifs);
viewManager.addViewMapping(DETAILS, renderGifDetails);

export const loadView = (view, params = null) => {
  viewManager.execute(view, params);
};
