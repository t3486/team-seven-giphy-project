import { NotificationManager } from '../views/NotificationManager.js';
import { uploadGif } from '../requests/requests-service.js';
import { addUploadedGif } from './helpers.js';

export const handleUpload = () => {
  const files = Array.from( document.getElementById('gif-upload-input').files);
  const notification = new NotificationManager();

  if (files.length === 0) return notification.showWarning('You should have selected a file!');

  uploadGif(files[0])
    .then((gif) => {
      addUploadedGif(gif.data.id);
      notification.showSuccess('GIF successfully uploaded :)');
    }).catch((error) => notification.showWarning(error.message));
};
