import { loadView } from './events/navigation-events.js';
import { HOME, INFINITE } from './common/constants.js';
import { select, toggleHeart } from './events/helpers.js';
import { handleUpload } from './events/upload-events.js';

document.addEventListener('DOMContentLoaded', () => {
  document.addEventListener('click', event => {
    // loading views
    if (event.target.classList.contains('nav-link')) {
      loadView(event.target.dataset.page);
    }
    // adding/removing from favorite
    if (event.target.classList.contains('like-heart')) {
      toggleHeart(event.target.dataset.gifId);
    }
    // uploading gifs
    if (event.target.classList.contains('upload-btn')) {
      event.preventDefault();
      handleUpload();
    }
    // display gif details
    if (event.target.classList.contains('gift-image')) {
      loadView(event.target.dataset.page, event.target.dataset.gifId);
    }
  });

  // display searched gifs
  select('#search-btn').addEventListener('click', (event) => {
    event.preventDefault();
    loadView(event.target.dataset.page);
  });

  loadView(HOME);
});

document.addEventListener('scroll', (ev) => {
  const {
    scrollTop,
    scrollHeight,
    clientHeight
  } = document.documentElement;

  if (scrollTop + clientHeight >= scrollHeight - 25) {
    
    if ([...select('#main-container').children][3].classList.contains('infinite-section')) {
      loadView(INFINITE);
    }
  }
}, {
  passive: true
});
