export class Swiped {

    #backButtons = null;
    #nextButtons = null;

    constructor() {}

    #getButtons() {
      this.backButtons = document.querySelectorAll('[data-swiped-back]');
      this.nextButtons = document.querySelectorAll('[data-swiped-next]');
    }

    attachEvents() {
      this.#getButtons();
      this.#backButtons.forEach(back => this.#backButtonEvent(back));
      this.#nextButtons.forEach(next => this.#nextButtonEvent(next));
    }

    #backButtonEvent(back) {
      back.addEventListener('click', () => {
        const swiped = document.getElementById(back.getAttribute('data-swiped-back'));
        swiped.children[0].scrollBy(swiped.children[0].children[0].clientWidth * -1, 0);
      });
    }

    #nextButtonEvent(next) {
      next.addEventListener('click', () => {
        const swiped = document.getElementById(next.getAttribute('data-swiped-next'));
        swiped.children[0].scrollBy(swiped.children[0].children[0].clientWidth, 0);

      });
    }

    // setters and getters
    set backButtons(value) {
      this.#backButtons = value;
    }

    set nextButtons(value) {
      this.#nextButtons = value;
    }
}
